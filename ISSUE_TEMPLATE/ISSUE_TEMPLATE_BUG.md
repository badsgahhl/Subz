---

name: "Bug Template"
about: "This template is only for bugs."
labels:

- bug

---

Thanks for using this issue template! :-)

## Description

Add a short description here.

## Version info

- Version code: X.X
- Got it from: F-Droid | Compiled from source | ...

## Steps to Reproduce

1. First
2. Second
3. Third

## Expected Results

What did you expect after the previous steps?

## Actual Results

What happened after the previous steps? You can also add images, videos, etc. 

## Other

Feel free to add things which didn't match to the prior headings.